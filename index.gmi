```
 __  __                           _                 _                    
|  \/  | ___ _ __ ___   ___  _ __(_) __ _ ___    __| | ___   _   _ _ __  
| |\/| |/ _ \ '_ ` _ \ / _ \| '__| |/ _` / __|  / _` |/ _ \ | | | | '_ \ 
| |  | |  __/ | | | | | (_) | |  | | (_| \__ \ | (_| |  __/ | |_| | | | |
|_|  |_|\___|_| |_| |_|\___/|_|  |_|\__,_|___/  \__,_|\___|  \__,_|_| |_|
                                                                         
 _____  _            _             _     _                      
|_   _|//  ___ _ __ (_) ___ ___   | |   (_) __ _  ___ _ __ ___  
  | |/ _ \/ __| '_ \| |/ __/ _ \  | |   | |/ _` |/ _ \ '__/ _ \ 
  | |  __/ (__| | | | | (_| (_) | | |___| | (_| |  __/ | | (_) |
  |_|\___|\___|_| |_|_|\___\___/  |_____|_|\__, |\___|_|  \___/ 
                                           |___/
```

# Esta es la versión alternativa capsulesca de las Memorias de un Técnico Ligero. 

Esta opción aboga por una web más sencilla, como la de antaño, enfocada en el contenido y no en hacer dinero con textos pobres, sin profundidad. No estoy diciendo con esto que no existan en el protocolo HTTP contenidos decentes y buenos, para nada. Me refiero a que el control de GAFAM ha estado decidiendo muchas cosas de las cuales no estoy de acuerdo, y veo que no estoy tan equivocado, porque hay gente que creó este protocolo Gémini y las cápsulas evolucionadas de Gopher. 

 Acá vas a poder encontrar algunos textos perdidos que no sé bien para donde van a ir.

Por cierto, a modo de reseña, he mudado mi autoalojamiento de una Raspberry Pi 4 Model B a una Raspberry Zero 2w, mucho más humilde en prestaciones pero sin dudas más que suficiente para este sitio y otros que tengo, estáticos (bajo el protocolo http) creados con el generador Pelican para Python.


### 📰 Noticias de último momento:
Ahora estoy en el listado de 'bot en deriva':
=>gemini://caracolito.mooo.com/deriva/ Bot en deriva. ¡Gracias Sejo por agregarme!

## 📚 Cuestiones de bitácora (Gemlog)

Esta sección reúne las cosas o bien que fui publicando en el blog bajo el protocolo HTTP o cuando me saque eso de encima como si fuera una enfermedad, recordarlo como algo que hice hace millones de años, en los albores de mis comienzos para volver al comienzo.
También voy a poner algunos esbozos o intentos de escritura. Algunos me dejan conforme, los menos, los más, se puede mejorar... digo esto y algún día... algun día...

* Últimas 5 entradas:
=>/gemlog/2024/29--2024-12-02-Generando-un-modo-kiosco-en-la-raspberry-que-ande.gmi 2024-12-02 Generando un modo kiosco en la raspberry que ande
=>/gemlog/2024/28--2024-10-14-El-problema-de-las-tarjetas-SD-de-poca-monta.gmi 2024-10-14 El problema de las tarjetas SD de poca monta
=>/gemlog/2024/27--2024-09-01-Copiando-un-directorio-con-SCP-a-pura-terminal.gmi 2024-09-01 Copiando un directorio con SCP a pura terminal
=>/gemlog/2024/26--2024-08-31-Probando-script-para-escribir-en-forma-dual-el-blog-HTTP-Gemini.gmi 2024-08-31 Probando script para escribir en forma dual el blog HTTP Gemini
=>/gemlog/2024/25--2024-08-26-Directorio-de-Bibliotecas-Libres.gmi 2024-08-26 Directorio de Bibliotecas Libres

=>gemlog/info.gmi 📝 Listado completo de entradas

## Cuestiones de técnico ligero (serían como las notas preliminares): 

=> info/el-camino-del-script.gmi El camino del Script. Mis scripts en Python.
=> info/geliba.gmi Un texto sobre mi aplicación Geliba hecha en Python.
=> info/mi-camino-al-software-libre.gmi Mi camino al Software Libre.-

* Este es el sitio con el protocolo HTTP:
=> https://tecnico-ligero.enlacepilar.com.ar

## 📩 Cuestiones de ¿contacto quizás?:

=>mailto:enlacepilar@protonmail.com  Correo electrónico.



