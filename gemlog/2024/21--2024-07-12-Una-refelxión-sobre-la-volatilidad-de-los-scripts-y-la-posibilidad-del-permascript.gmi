#Una refelxión sobre la volatilidad de los scripts y la posibilidad del permascript

Me encanta hacer scripts. Después de haber pasado por muchas instancias de mi vida, podría decir sin temor a equivocarme que me quedo con los libros (la lectura) y los scripts, de todas las actividades que he hecho, claro. Es más, hasta dejo relegado el Aikido que tanto me ha ayudado, pero sin dudas los scripts han venido a traerme salud mental en los momentos de ocio, en los momentos de tribulaciones, en los momentos en que quería ser un fallido y malogrado programador web: claro que eso también queda de lado, del mismo modo ser un técnico que atiende usuarios, eso tampoco es saludable y queda relegado.

Cabe aclarar querido lector, si es que existe uno del otro lado que se sumerge en estas líneas, que muy lejos estoy de profundizar en "las grandes ligas de los scripts" haciendo cosas muy cerca de la matrix o lo binario; no. Más bien podría decirse que soy de una liga amateur, de un pueblo perdido en el medio de la nada.
Sin embargo no vengo aquí para hablar de los logros que produce automatizar, jugar o probar con algunos de estos scripts, preferentemente de Python; de una cuestión que tiene que ver con el arte, con lo que me acerca a mí como persona, lo que me conecta a este mundo informático, de Software Libre y protocolo Gèmini, sin irme demasiado por las ramas. 

El script es lo más volátil que puede haber: una versión trunca y se romperá, una línea nueva en el destino y se romperá, una búsqueda restringida con "os.walk" y ya no podremos avanzar, una base de datos migrada y lejos estará de servir (o actualizada). Entonces, amigos, busquemos el permascript, el script que permanezca en el tiempo con ínfimos recursos. Sin ir más lejos este no lo es, ya qye estoy escribiendo desde un script de python, en una PC remota hacia mi Raspberry Pi Zero 2w, pero un solo cambio en la ruta cambiará todo, entonces hay que modificar, casi tanto como los enlaces truncos, otro flagelo de internet y lo efímero, pero eso queda para otra reflexión. Por lo pronto, he de buscar el script que pueda perdurar, cual alquimista busca el elixir de la vida, o la piedra filosofal, me pregunto si Fulcanelli lo habrá encontrado...

* *** RUTAS ESPACIALES ***
=>/gemlog/info.gmi volver al listado de escritos
=>../../ volver al inicio
