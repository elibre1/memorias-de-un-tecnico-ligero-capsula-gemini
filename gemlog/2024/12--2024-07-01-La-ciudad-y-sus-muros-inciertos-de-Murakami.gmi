#La ciudad y sus muros inciertos de Murakami

Estoy leyendo este libro y la verdad que Murakami no deja de sorprenderme, no sé si está escrbiendo pensando en mí o yo soy un personaje Murakami. Estoy en estas etapas de la vida en que quiero dejar la informática para irme a trabajar a una biblioteca, tengo 45 (casi como el personaje) y si me pudiera ir a un pueblito perdido mucho mejor, es así, con la salvedad de que es un deseo y no he de poder hacerlo de momento porque tengo un niño al cual enseñarle algunas cosillas hasta que despegue y haga su vida. 

Pero vayamos un poco a lo que nos concierne. Se me mezcla todo, paseando por los senderos ribereños de Trevelin me imagino el rio serpenteante del personaje, similar; estuve en verano por allá y me encantó, más que Esquel. 

La biblioteca... puedo respirar el olor de los libros, la mesa de madera larga... me hace recordar cuando era chico e iba a la Madero de San Fernando a estudiar, qué maravilla. El café en el bar de la chica... las 'madalenas' o muffins, todo eso que estoy leyendo. Me quedan algo más de 100 páginas, ya casi lo termino. Es un bodoque interesante. 


* *** RUTAS ESPACIALES ***
=>/gemlog/info.gmi volver al listado de escritos
=>../../ volver al inicio
