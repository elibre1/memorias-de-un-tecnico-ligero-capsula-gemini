# Lo efímero...
22-06-24

Pienso un poco en lo efimero de la web, en base a algunas lecturas... Todo esto se perderá en algún momento, somos como pedazos de bits en el tiempo corto y volátil, a lo mejor si escribís algo interesante se podrá reproducir por alguien más y perdurar un poco más... pero invariablemente estamos condenados a perderlo todo, como la vida misma amigo, así funciona la cosa.

Entonces para qué esforzarse? Porque sí, porque es interesante, porque está bueno crear, porque está bueno completar el destino con alguna cosilla. Alguien puede venir y leer, motivarse y crear lo suyo sin que siquiera te mencione, pero sivió. Tampoco estoy pensando en beneficiarme haciendo dinero como lo hacen las grandes empresas o algunos trasnochados que sueñan ganar dinero como ellas, hay que sacrificar un pedazo de tu libertad para eso y transformarte en otra cosa. Eso no lo quiero para mí. 
Quiero ser libre y darle duro al teclado si me da la gana, motivado por otras lecturas inspiradoras... y a lo mejor en el transcurso de un tiempo completamente diferente al que pienso, lineal, concreto, pueda ver que otro intenta algo mejor.


* *** RUTAS ESPACIALES ***
=>/gemlog/info.gmi volver al listado de escritos
=>../../ volver al inicio
