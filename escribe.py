import os
from datetime import datetime

nom_directorio = 'gemlog' #esto se podría cambiar por input=('ingrese nombre de directorio')
lista_datos = []
documento = []

#Obtengo algunas fechas para trabajar luego
actual = datetime.now()
fecha_actual = actual.date()
año = fecha_actual.strftime("%Y")


archivos = []
for ruta, directorio, archivos in os.walk(nom_directorio):
    archivos.sort(reverse=True)
ultima_publicacion = archivos[0]
#print (ultima_publicacion)
nro_publicacion = int(ultima_publicacion[:2])+1
titulo = input ("Ingresar titulo de la publicación:\n")
titulo_archivo = titulo.replace(" ", "-")
#print (titulo_archivo)
titulo_archivo = str(nro_publicacion)+'--'+str(fecha_actual)+"-"+titulo_archivo+".gmi"
#print ("el titulo del archivo sería: \n"+ titulo_archivo)

texto_publicacion = input ("Ingresar el texto de la publicación: \n")

sal = input ("carga mas en el texto (enter) (X para salir)")
while sal != "X":
    texto_publicacion +="\n"
    texto_publicacion += input ("texto: ")
    sal = input ("carga mas en el texto (enter) (X para salir)")
                          
#Escribo el index con el contenido de la lista documento:
with open(os.getcwd()+'/'+nom_directorio+"/"+año+"/"+titulo_archivo, 'a', encoding='utf-8') as nuevo_index:
    nuevo_index.write("#"+titulo+"\n\n")
    nuevo_index.write(texto_publicacion)
    nuevo_index.write("\n\n* *** RUTAS ESPACIALES ***\n")
    nuevo_index.write("=>/gemlog/info.gmi volver al listado de escritos\n")
    nuevo_index.write("=>../../ volver al inicio\n")
    nuevo_index.close()
                 
consulta = input ("¿Actualiza la pag. principal? S/N")

if consulta == 'S' or consulta == 's':
    exec(open("regenera.py").read())
else:
    print ("¡Todo creado sin actualizar principal!")
