import os

nom_directorio = 'gemlog' #esto se podría cambiar por input=('ingrese nombre de directorio')
lista_datos = []
documento = []

for ruta, directorio, archivos in os.walk(nom_directorio):
    archivos.sort(reverse=True)
    for elemento in archivos:
        if 'info.gmi' not in archivos:
            fecha_string = elemento[4:14] 
            titulo = elemento[15:-4].replace('-', ' ')
            lista_datos.append('=>/'+str(ruta)+'/'+str(elemento)+" "+fecha_string+" "+titulo+"\n")


##########################################################################
#Voy a generar el index de la página principal a partir del index_base
#que tengo con el formato
##########################################################################
            
archivo_base = open ('index_base.gmi', 'r', encoding='utf-8')
documento = archivo_base.readlines()
archivo_base.close()

#Voy a seccionar para la página principal solo las primeras 5 entradas:
ultimas_cinco_entradas= lista_datos[:5]

for i in range(len(documento)):
    if 'Últimas 5 entradas' in documento[i]:
        for linea1 in ultimas_cinco_entradas:
            documento[i] += linea1

#Escribo el index con el contenido de la lista documento:
with open('index.gmi', 'w') as nuevo_index:
    for linea2 in documento:
        nuevo_index.write(linea2)
    nuevo_index.close()

##########################################################################
#Ahora voy a escribir el info de la sección gemlog, con todas las entradas
##########################################################################

archivo_base_entradas = open ('gemlog/info_base.gmi', 'r', encoding='utf-8')
documento_entradas = archivo_base_entradas.readlines()
archivo_base_entradas.close()

for i in range(len(documento_entradas)):
    if 'Listado por fecha:' in documento_entradas[i]:
        for linea1 in lista_datos:
            documento_entradas[i] += linea1

#Escribo el index con el contenido de la lista documento:
with open('gemlog/info.gmi', 'w') as nuevo_index2:
    for linea2 in documento_entradas:
        nuevo_index2.write(linea2)
    #nuevo_index2.write('\n=>../ Volver al inicio')
    nuevo_index2.close()

print ("¡Todo actualizado!")
