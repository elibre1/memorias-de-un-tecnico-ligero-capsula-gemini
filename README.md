```
 __  __                           _                 _                    
|  \/  | ___ _ __ ___   ___  _ __(_) __ _ ___    __| | ___   _   _ _ __  
| |\/| |/ _ \ '_ ` _ \ / _ \| '__| |/ _` / __|  / _` |/ _ \ | | | | '_ \ 
| |  | |  __/ | | | | | (_) | |  | | (_| \__ \ | (_| |  __/ | |_| | | | |
|_|  |_|\___|_| |_| |_|\___/|_|  |_|\__,_|___/  \__,_|\___|  \__,_|_| |_|
                                                                         
 _____               _             _     _                      
|_   _|__  ___ _ __ (_) ___ ___   | |   (_) __ _  ___ _ __ ___  
  | |/ _ \/ __| '_ \| |/ __/ _ \  | |   | |/ _` |/ _ \ '__/ _ \ 
  | |  __/ (__| | | | | (_| (_) | | |___| | (_| |  __/ | | (_) |
  |_|\___|\___|_| |_|_|\___\___/  |_____|_|\__, |\___|_|  \___/ 
                                           |___/
```

###Esta es la versión alternativa de las Memorias de un Técnico Ligero. 

Esta opción aboga por una web más sencilla, como la de antaño, enfocada en el contenido y no en hacer dinero con texto pobres, sin profundidad. No estoy diciendo con esto que no exista en el protocolo HTTP contenidos decentes y buenos, para nada. Me refiero a que el control de GAFAM ha estado decidiendo muchas cosas de las cuales no estoy de acuerdo, y veo que no estoy tan equivocado, porque hay gente que creó este protocolo Gémini y las cápsulas evolucionadas de Gopher 

Acá vas a poder encontrar algunos textos perdidos que no sé bien para donde van a ir.

Por cierto, a modo de reseña, he mudado mi autoalojamiento de una Raspberry Pi 4 Model B a una Raspberry Zero 2w, mucho más humilde en prestaciones pero sin dudas más que suficiente para este sitio y otros que tengo, estáticos (bajo el protocolo http) creados con el generador Pelican para Python.


* Este es el sitio con el protocolo Gemini:
[Memorias de un técnico ligero encapsuladito] (gemini://tecnico-ligero.enlacepilar.com.ar)

* Este es el sitio con el protocolo HTTP:
[Memorias de un técnico ligero en HTTP] (https://tecnico-ligero.enlacepilar.com.ar)


"Lo que he hecho hasta ahora no fue un error. Fue necesario para llegar hasta aquí."
